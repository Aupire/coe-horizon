import logging
from datetime import timedelta
from random import randint

from django.utils import timezone

from extra_mail.models import Message
from hacking.models import HackAttempt
from program.models import InstalledProgram, ProgramType

def VC(attempt):
    pass

def AR(attempt):
    """Analyse des risques"""
    attempt.detection_chance = attempt.detection_chance - 0.03
    if attempt.detection_chance < 0:
        attempt.detection_chance = 0

    attempt.save()

def VI(attempt):
    """Vulnérabilité informatique"""
    target_installed_programs = attempt.target.programs_installed.all()
    firewalls = target_installed_programs.filter(program__id_code__contains='PF')

    number_installed = firewalls.count()
    if number_installed > 0:
        random_index = randint(0, number_installed - 1)
        to_delete = firewalls.all()[random_index]

        to_delete.delete()

    hacker = attempt.hacker
    vi = ProgramType.objects.get(id_code='VI')
    installed_vi = InstalledProgram.objects.get(target=hacker, program=vi)
    installed_vi.delete()

def C1(attempt):
    target = attempt.target
    target.encrypted = True
    target.encrypted_until = timezone.now() + timedelta(hours=2)
    target.save()

    c1 = ProgramType.objects.get(id_code='C1')
    installed_c1 = InstalledProgram.objects.get(target=attempt.hacker, program=c1)
    installed_c1.delete()

def PFA3(attempt):
    attempt.detection_chance = 1
    attempt.save()

    target = attempt.target
    pfa3 = ProgramType.objects.get(id_code='PFA3')

    firewall = InstalledProgram.objects.get(target=target, program=pfa3)
    firewall.delete()

def PFI(attempt):
    hacker = attempt.hacker
    target = attempt.target
    pfi = ProgramType.objects.get(id_code='PFI')

    firewall = InstalledProgram.objects.get(target=target, program=pfi)
    effect_target = firewall.effect_target

    message = "Piratage détecté de la part d'un membre de l'organisation suivante: " + hacker.corporation.__str__()

    message = Message(
        by=target,
        subject="Alerte Automatique de: \"Pare-feu détecteur d'intrusion\"",
        content=message,
    )
    message.save()
    message.to.add(effect_target)

    message.save()
    firewall.delete()

def PFH(attempt):
    hacker = attempt.hacker
    target = attempt.target
    pfh = ProgramType.objects.get(id_code='PFH')

    firewall = InstalledProgram.objects.get(target=target, program=pfh)
    effect_target = firewall.effect_target

    message = "Piratage détecté et bloqué.\n\nLe pirate informatique à été identifié: " + hacker.__str__()

    message = Message(
        by=target,
        subject="Alerte Automatique de: \"Pare-feu honeypot\"",
        content=message,
    )
    message.save()
    message.to.add(effect_target)

    message.save()
    firewall.delete()
