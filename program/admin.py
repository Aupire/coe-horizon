from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.ProgramType)
admin.site.register(models.InstalledProgram)
