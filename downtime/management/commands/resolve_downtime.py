import numpy as np
from django.core.management.base import BaseCommand

from character.models import Character
from character.utils import char_has_skill, get_generic_account, get_paid_chars_last_larp
from downtime.models import Action, Sector, Tick
from program.models import InstalledProgram
from datetime import datetime
from django.db.models import Q

from extra_mail.models import Message


def resolve_sectors():
    current_tick = Tick.objects.get(calc_done=False)
    current_actions = Action.objects.filter(tick=current_tick)
    notification_account = get_generic_account("HORS_JEU_CONQUÊTES")
    subject = "[HORS JEU] Tentative de conquête"

    for sector in Sector.objects.all():
        if sector.controlling_character:
            sector.previously_controlled_by.add(sector.controlling_character)
            sector.save()

        # Reset sector
        sector.bonus_vd = 0
        sector.saboted = False

        # Defense Actions
        defense_actions = current_actions.filter(sector=sector).filter(Q(action_type = Action.DEFENCE) | Q(action_type=Action.SECURITY_SYSTEM))
        for action in defense_actions:
            sector.bonus_vd += action.total_influence

        # Sector Control custom actions
        if sector.name == 'Main d\'oeuvre : Ouvriers':
            sector.bonus_vd += Action.objects.filter(tick=current_tick, reduces_workers_vd=True).count() * -1
        if sector.name == 'Militaire':
            sector.bonus_vd += Action.objects.filter(tick=current_tick, reduces_military_vd=True).count() * -1

        sector.save()
        sector.refresh_from_db()

        # Attack actions
        conquest_actions = current_actions.filter(action_type=Action.CONQUEST, sector=sector)
        conqueror = None
        runner_up = None
        for action in conquest_actions:
            if sector.controlling_character:
                # Advise target of conquests
                message = Message(
                    by=notification_account,
                    subject=subject,
                    content=f"Une tentative de conquête sur le secteur {sector.name} à été détectée de la part de la corporation suivante: {action.character.corporation.name}"
                )
                message.save()
                message.to.add(sector.controlling_character)
                message.save()

            if action.total_influence >= sector.current_vd:
                if conqueror is None:
                    conqueror = action
                elif action.total_influence >= conqueror.total_influence:
                    runner_up = conqueror
                    conqueror = action

        # Check if we have a winner
        if conqueror is not None:
            if runner_up is None or (runner_up is not None and conqueror.total_influence > runner_up.total_influence):
                sector.controlling_character = conqueror.character
            if runner_up and conqueror.total_influence == runner_up.total_influence:
                sector.controlling_character = None

        # Sabotage Actions
        sabotage_actions = current_actions.filter(action_type=Action.SABOTAGE, sector=sector)
        for action in sabotage_actions:
            if action.total_influence >= sector.current_vd:
                sector.saboted = True

        # Saving
        sector.save()

    current_tick.calc_done = True
    current_tick.save()

def run_financial_analysts():
    financial_analysts = InstalledProgram.objects.filter(program__id_code='AF')

    for af in financial_analysts:
        mult = 0.05

        if af.target.corporation.name == 'Groupe Insta-Vie':
            mult = 0.1

        account = af.target.bank_account
        account.investment_balance += int(account.balance * mult)
        account.save()

def level_up_players():
    paid_chars = get_paid_chars_last_larp()

    for char in paid_chars:
        if char.level < 9:
            char.level += 1
        else:
            char.deactivated = True

        char.save()


def spend_influence():
    tick = Tick.objects.get(calc_done=False)
    actions = Action.objects.filter(tick=tick).exclude(action_type=Action.SECURITY_SYSTEM)
    chars = []

    for action in actions:
        chars.append(action.character)

    chars = list(dict.fromkeys(chars))
    for char in chars:
        char.spend_influence()


def gen_influence():
    tick = Tick.objects.get(calc_done=False)
    actions = Action.objects.filter(tick=tick,
                                    action_type=Action.GENERATION)
    chars = []

    for action in actions:
        chars.append(action.character)

    chars = list(dict.fromkeys(chars))
    for char in chars:
        char.gen_influence()


def heal_conditions():
    current_tick = Tick.objects.get(calc_done=False)
    actions = Action.objects.filter(tick=current_tick).exclude(healed_condition=None)

    for action in actions:
        condition = action.healed_condition
        condition.healed_at = datetime.now()
        condition.save()

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        heal_conditions()
        spend_influence()
        gen_influence()
        level_up_players()
        resolve_sectors()
        run_financial_analysts()
