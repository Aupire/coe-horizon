from character.models import Character, Skill
from character.utils import char_has_skill
from django.db import models
from django.db.models import Sum
from larpdate.models import LarpDate
from craft.models import CraftRecord
from conditions.models import AppliedCondition


# Create your models here.
class Sector(models.Model):
    name = models.CharField(max_length=50)
    base_vd = models.IntegerField()
    bonus_vd = models.IntegerField(default=0)
    controlling_character = models.ForeignKey(Character, on_delete=models.PROTECT, null=True, blank=True)
    saboted = models.BooleanField(default=False)
    previously_controlled_by = models.ManyToManyField(Character, related_name='previously_controlled_by')

    @property
    def current_vd(self):
        return self.base_vd + self.bonus_vd

    def __str__(self):
        return self.name

class Tick(models.Model):
    larp = models.ForeignKey(LarpDate, on_delete=models.PROTECT)
    completed = models.BooleanField(default=False)
    calc_done = models.BooleanField(default=False)

class Assistance(models.Model):
    action = models.ForeignKey('Action', on_delete=models.CASCADE)
    character = models.ForeignKey(Character, on_delete=models.PROTECT)
    active_participation = models.BooleanField(default=False)
    influence_invested = models.IntegerField(default=0)
    declined = models.BooleanField(default=False)

    @property
    def uses_downtime_action(self):
        return self.active_participation

    def __str__(self):
        return self.character.__str__() + " assists:" + self.action.__str__()

class Action(models.Model):
    CONQUEST = 'CQ'
    DEFENCE = 'DEF'
    SABOTAGE = 'SAB'
    ASSISTANCE = 'AS'
    OPERATION = 'OP'
    CRAFT = 'CR'
    AUGMENTATION = 'AUG'
    GENERATION = 'GEN'
    SHOP = 'SH'
    MILITARY_SECTOR = 'MIL'
    SECURITY_SYSTEM = 'SEC'
    TYPES = [
        (CONQUEST, 'Conquête'),
        (DEFENCE, 'Défense'),
        (SABOTAGE, 'Sabotage'),
        (ASSISTANCE, 'Assistance'),
        (OPERATION, 'Opération'),
        (CRAFT, 'Fabrication d\'objet'),
        (AUGMENTATION, 'Installation d\'augmentations'),
        (GENERATION, 'Générer de l\'infuence avec une compétence'),
        (SHOP, 'Acheter ou vendre des objets'),
        (SECURITY_SYSTEM, 'Utiliser la compétence "Systèmes de sécurité"'),
        (MILITARY_SECTOR, '(Secteur Militaire) Soigner une condition'),
    ]
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    tick = models.ForeignKey(Tick, on_delete=models.PROTECT)
    action_type = models.CharField(max_length=3, choices=TYPES, default=CONQUEST)
    influence_invested = models.IntegerField(default=0)
    invited_characters = models.ManyToManyField(Character, related_name='invited')

    operation_name = models.CharField(max_length=100, null=True, blank=True)
    operation_description = models.TextField(null=True, blank=True)
    operation_objective = models.TextField(null=True, blank=True)
    operation_pertinent_stats = models.TextField(null=True, blank=True)
    sector = models.ForeignKey(Sector, on_delete=models.PROTECT, null=True, blank=True)
    assisting = models.ForeignKey(Assistance, on_delete=models.CASCADE, null=True, blank=True, related_name='assisting')
    craft_record = models.ForeignKey(CraftRecord, on_delete=models.PROTECT, null=True, blank=True)
    shop_skill = models.ForeignKey(Skill, on_delete=models.PROTECT, null=True, blank=True, related_name='shop_skill')
    approval_required = models.BooleanField(default=False)
    approved_by_anim = models.BooleanField(default=False)
    is_skill_free_action = models.BooleanField(default=False)
    free_action_skill_used = models.ForeignKey(Skill, on_delete=models.PROTECT, null=True, blank=True, related_name='free_action_skill')
    anim_answer = models.TextField(null=True, blank=True)
    answer_completed = models.BooleanField(default=False)
    answer_sent = models.BooleanField(default=False)

    reduces_military_vd = models.BooleanField(default=False)
    reduces_workers_vd = models.BooleanField(default=False)

    healed_condition = models.ForeignKey(AppliedCondition, on_delete=models.CASCADE, null=True, blank=True)


    def __str__(self):
        sector_actions = [self.CONQUEST, self.DEFENCE, self.SABOTAGE]
        try:
            if self.action_type in sector_actions:
                return f'{self.character.full_name} - {self.get_action_type_display()}: {self.sector.name}'
            elif self.action_type == self.MILITARY_SECTOR:
                return f'{self.character.full_name} - SOIGNER CONDITION'
            elif self.action_type == self.ASSISTANCE:
                return f'{self.character.full_name} - ASSISTANCE - {self.assisting.action.__str__()}'
            elif self.action_type == self.OPERATION:
                return f'{self.character.full_name} - OPERATION: {self.operation_name} ( {self.total_influence} influence) '
            elif self.action_type == self.CRAFT:
                try:
                    return f'{self.character.full_name} - FABRICATION - {self.craft_record.recipe}'
                except AttributeError:
                    return f'{self.character.full_name} - FABRICATION - Non Configuré'
            else:
                return f'{self.character.full_name} - {self.get_action_type_display()}'
        except:
            return f'{self.character.full_name} - {self.get_action_type_display()} - NON-CONFIGURÉ'

    @property
    def uses_downtime_action(self):
        is_assistance = self.action_type == self.ASSISTANCE
        if is_assistance:
            return False
        else:
            return not self.is_skill_free_action

    @property
    def active_participants(self):
        assistances = Assistance.objects.filter(action=self, active_participation=True)
        participants = f'{self.character} ({self.character.player.first_name} {self.character.player.last_name})'

        for assistance in assistances:
            participants += f', {assistance.character} ({assistance.character.player})'

        return participants

    @property
    def total_influence(self):
        assistances = Assistance.objects.filter(action=self)
        malus = 0
        bonus = 0

        if char_has_skill(self.character, 'Alliés clandestins') and self.sector and self.character in self.sector.previously_controlled_by.all() and self.action_type == self.CONQUEST:
            bonus += 1

        if self.character.origin.name == 'Aristocratie corporative':
            malus += 1

        if char_has_skill(self.character, 'Alter-Ego Virtuel') and self.action_type == Action.SABOTAGE:
            bonus += 2

        if self.action_type == Action.DEFENCE and self.reduces_workers_vd:
            bonus += 1

        if self.action_type == Action.SABOTAGE and self.reduces_military_vd:
            bonus += 1

        if len(assistances) > 0:
            return self.influence_invested + assistances.aggregate(
                Sum('influence_invested'))['influence_invested__sum'] - malus + bonus
        else:
            return self.influence_invested + bonus
