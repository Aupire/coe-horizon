# Initial setup

Those instructions are written for a Linux installation, assuming python3.9 is already installed

```
# Create a venv
python3.9 -m venv horizon
source horizon/bin/activate

# Install packages
# You might need to install the package containing pg_config for your distribution:
# Ubuntu/Debian: libpq-dev
# CentOS/Fedora: libpq-devel
# Arch/Manjaro:  postgresql-libs

pip install -r requirements.txt


# Setup local DB
python manage.py migrate

# Load Skills from website
python manage.py parse

# Create local admin account
python manage.py createsuperuser

# Start local server
python manage.py runserver
```

You should then be able to access your local extranet at http://localhost:8000

# Contributing
To contribute, you can open a PR by first forking using those instructions:
https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
