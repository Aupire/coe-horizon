from django.core.management.base import BaseCommand

from character.models import Skill
from craft.gsheet import retreive_sheet_data
from craft.models import Recipe, RecipeCategory


def get_recipe_category(category):
    try:
        recipe_category = RecipeCategory.objects.get(name=category)
    except:
        recipe_category = RecipeCategory(name=category)
        recipe_category.save()

    return recipe_category



class Command(BaseCommand):
    help = 'Parses craft google sheet and puts result in recipes'

    def handle(self, *args, **kwargs):
        sheet_names = [
            'Armurerie',
            'Balistique',
            'Bioingénierie',
            'Biotechnologie',
            'Cybernétique',
            'Matériaux',
            'Mécatronique',
            'Pharmacologie',
            'Programmation',
            'Protection Matérielle',
            'Robotique',
            'Gestionnaire',
            'Ingénierie Spécialisée',
        ]

        for category in sheet_names:
            df = retreive_sheet_data(category)

            description_column = [i for i in df.columns if 'Description' in i][0]
            keydict = {
                'Identification': 'identification',
                'Emplacement': 'slot',
                'Pré-Requis': 'prerequisite',
                'Si défectueux (en plus de la perte de bonus)': 'defective_effect',
                'Prix du marché': 'market_price',
                'Fabrication : Crédits': 'craft_price',
                'Fabrication : Ressources': 'resource_cost',
                'Fabrication : Étapes': 'craft_step',
                'Temps (minutes)': 'craft_time',
                'Temps (downtime)': 'downtime_cost',
                'Max par Downtime': 'max_per_downtime',
                description_column: 'description',
            }

            values_to_skip = ['-', '']
            for index, row in df.iterrows():

                names_to_skip = [None, 'Nom', '']
                if row['Nom'] not in names_to_skip:
                    # Get or create recipe object
                    try:
                        recipe = Recipe.objects.get(name=row['Nom'])
                    except:
                        recipe = Recipe(name=row['Nom'])

                    # Update all data if that is available
                    recipe.category = get_recipe_category(category)

                    for df_key, attribute in keydict.items():
                        try:
                            value = row[df_key]

                            if not (value in values_to_skip or value is None):
                                if attribute == 'market_price':
                                    # If it's not an int, we'll raise an exception and skip
                                    value = int(value)

                                setattr(recipe, attribute, value)
                        except:
                            # We don't have this data in the current row, skipping
                            pass

                    try:
                        # If we have manually set category, let's override it.
                        recipe.category = get_recipe_category(row['Compétence'])
                    except:
                        pass

                    # We got the recipe from google sheets, so it's public!
                    recipe.is_public = True
                    recipe.save()

            # Hide Geology items (as they cannot be crafted)
            for recipe in Recipe.objects.filter(category__name='Géologie'):
                recipe.is_public = False
                recipe.save()

        # Ensure all categories have proper skill prerquisite
        category_prereq_dict = {
            'Armurerie': Skill.objects.get(name='Armurerie - Innovation'),
            'Balistique': Skill.objects.get(name='Balistique - Innovation'),
            'Bioingénierie': Skill.objects.get(name='Bioingénierie - Innovation'),
            'Biotechnologie': Skill.objects.get(name='Biotechnologie - Innovation'),
            'Cybernétique': Skill.objects.get(name='Cybernétique - Innovation'),
            'Mécatronique': Skill.objects.get(name='Mécatronique - Innovation'),
            'Pharmacologie': Skill.objects.get(name='Pharmacologie - Innovation'),
            'Programmation': Skill.objects.get(name='Programmation - Innovation'),
            'Protection Matérielle': Skill.objects.get(name='Protection matérielle'),
            'Robotique': Skill.objects.get(name='Robotique - Innovation'),
            'Nanotech': Skill.objects.get(name='Nanotech'),
            'Chimie': Skill.objects.get(name='Chimie - Innovation'),
            'Ingénierie Spécialisée': Skill.objects.get(name='Ingénierie spécialisée - Innovation'),
            'Gestionnaire': Skill.objects.get(name='Gestionnaire')
        }

        for category, skill in category_prereq_dict.items():
            recipe_category = RecipeCategory.objects.get(name=category)
            recipe_category.prerequisite_skill = skill
            recipe_category.save()
