# Generated by Django 2.2.4 on 2019-08-25 16:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('craft', '0002_auto_20190825_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='downtime_cost',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='recipe',
            name='max_per_downtime',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
