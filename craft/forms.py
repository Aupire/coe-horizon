from django import forms

from craft.models import CraftRecord
from bank_account.models import Transaction
from character.utils import char_has_skill, get_generic_account
from larpdate.utils import larp_is_active


def calc_recipe_final_cost(recipe, number_of_items):
    craft_price = recipe.craft_price
    if craft_price is None:
        craft_price = 0

    final_price = max(10, craft_price)

    return final_price * number_of_items


class CraftItemForm(forms.ModelForm):
    downtime = forms.BooleanField(
        initial=False,
        widget=forms.HiddenInput,
        required=False
    )

    class Meta:
        model = CraftRecord
        fields = ['recipe', 'number_of_items', 'crafter', 'extra_spend']
        labels = {
            'recipe': 'Objet',
            'extra_spend': '(Secteur Resources Naturelles) Crédits supplémentaires à utiliser:',
        }
        widgets = {
            'crafter': forms.HiddenInput(),
            'extra_spend': forms.HiddenInput(),
        }

    def clean(self):
        cleaned_data = super().clean()
        crafter = cleaned_data.get('crafter')
        recipe = cleaned_data.get('recipe')
        number_crafted = cleaned_data.get('number_of_items')

        if not larp_is_active() and not cleaned_data.get('downtime'):
            raise forms.ValidationError("L'activitée n'est pas actuellement en cours, veuillez réessayer pendant l'activité ou la session de downtime")

        if cleaned_data.get('downtime'):
            if number_crafted > recipe.max_per_downtime:
                raise forms.ValidationError(f'Cet objet peut-être fabriquer un maximum de {recipe.max_per_downtime} par action de Downtime!')

        prerequisite_skill = recipe.category.prerequisite_skill.name
        if not char_has_skill(crafter, prerequisite_skill):
            raise forms.ValidationError(f'Vous devez avoir la compétence "{prerequisite_skill}" pour fabriquer cet objet!')

        target = get_generic_account('Matériaux Divers')

        final_cost = calc_recipe_final_cost(recipe, number_crafted)

        transaction = Transaction(
            amount=final_cost + cleaned_data.get('extra_spend'),
            source=crafter.bank_account,
            destination=target.bank_account
        )

        try:
            transaction.save()
        except Exception:
            raise forms.ValidationError(
                f'Fonds inssufisants, au moins {final_cost}cr sont requis dans votre compte!'
            )
