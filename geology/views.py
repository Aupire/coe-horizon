from django.shortcuts import redirect, render
from django.utils import timezone

from character import views as char_views
from character.utils import get_character
from geology.forms import SelectPoleForm
from geology.models import Pole


# Create your views here.
def geology_overview(request):
    context = {}
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character
    context['poles'] = Pole.objects.filter(owner=character).order_by('collected', '-stopped_at')

    return render(request, 'geology/overview.djhtml', context)

def pole_qr(request, pole_id):
    if request.user.is_staff:
        pole = Pole.objects.get(id=pole_id)

        qr = {
            'security_token': pole.security_token,
            'pole_id': pole.id,
        }

        context = {
            'qr': qr,
            'pole_id': pole.id,
        }

        return render(request, 'geology/pole_qr.djhtml', context)
    else:
        # Fishy stuff, redirect to login
        return redirect(char_views.view_login)

def claim_pole(request):
    context = {'next': 'claim_pole'}
    context['page_title'] = "Réclamation d'un marqueur"

    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character

    if request.method == 'POST':
        form = SelectPoleForm(request.POST)
        if form.is_valid():
            pole_id = form.cleaned_data['pole_id']
            security_token = form.cleaned_data['security_token']

            pole = Pole.objects.get(id=pole_id, security_token=security_token)
            if pole.owner is None:
                pole.owner=character
                pole.save()
                return redirect(geology_overview)
            else:
                context['poles'] = Pole.objects.filter(owner=character)
                context['error'] = 'Le marqueur ciblé est déjà réclamé'
                return render(request, 'geology/overview.djhtml', context)

    context['form'] = SelectPoleForm()
    return render(request, 'geology/select_pole.djhtml', context)

def install_pole(request):
    context = {'next': 'install_pole'}
    context['page_title'] = "Installation d'un marqueur"

    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character

    if request.method == 'POST':
        form = SelectPoleForm(request.POST)
        if form.is_valid():
            pole_id = form.cleaned_data['pole_id']
            security_token = form.cleaned_data['security_token']

            pole = Pole.objects.get(id=pole_id, security_token=security_token)
            if not pole.started:
                pole.started = True
                pole.installed_at = timezone.now()
                pole.save()
                return redirect(geology_overview)
            else:
                context['poles'] = Pole.objects.filter(owner=character)
                context['error'] = 'Le marqueur ciblé est déjà installé'
                return render(request, 'geology/overview.djhtml', context)

    context['form'] = SelectPoleForm()
    return render(request, 'geology/select_pole.djhtml', context)


def collect_pole(request):
    context = {'next': 'collect_pole'}
    context['page_title'] = "Collecte d'un marqueur"

    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character

    if request.method == 'POST':
        form = SelectPoleForm(request.POST)
        if form.is_valid():
            pole_id = form.cleaned_data['pole_id']
            security_token = form.cleaned_data['security_token']

            pole = Pole.objects.get(id=pole_id, security_token=security_token)
            if not pole.collected:
                result = pole.collect()
                if result is None:
                    context['poles'] = Pole.objects.filter(owner=character)
                    context['error'] = 'La récolte ne fut pas compléter. Veuillez vérifier que vous avez suffisamment de crédits pour payer les frais (50cr)'

                return redirect(geology_overview)

            else:
                context['poles'] = Pole.objects.filter(owner=character)
                context['error'] = 'Le marqueur ciblé est déjà collecté'
                return render(request, 'geology/overview.djhtml', context)

    context['form'] = SelectPoleForm()
    return render(request, 'geology/select_pole.djhtml', context)
