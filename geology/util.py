import random
from decimal import Decimal

from geology.models import Mineral, Pole


class Prob():
    def __init__(self, lbound, ubound):
        self.lbound = lbound
        self.ubound = ubound

    def is_match(self, roll):
        return roll >= self.lbound and roll < self.ubound

def generate_prob_array(credit_amount, target_mineral):
    minerals = Mineral.objects.all()

    current_lbound = Decimal(0)
    current_ubound = Decimal(0)
    prob_array = []

    for i_mineral in minerals:
        if i_mineral.market_price <= credit_amount:
            chances = i_mineral.chance_to_spawn

            prob_array.append(
                (
                    Prob(current_lbound, current_lbound+chances),
                    i_mineral,
                )
            )

            current_lbound = current_lbound + chances


    if target_mineral.market_price <= credit_amount:
        prob_array.append(
            (
                Prob(current_lbound, current_lbound+Decimal(25)),
                target_mineral
            )
        )
        current_lbound = current_lbound + Decimal(26)

    return (prob_array, current_lbound)

def get_roll_result(roll, prob_array):
    for i in prob_array:
        if i[0].is_match(roll):
            return i[1]

    # We hit exacly ubound, returning last entry
    return prob_array[-1][1]

def generate_pole_results(credit_amount, target_mineral, first_run=True):
    result = []

    if first_run and credit_amount >= target_mineral.market_price:
        result.append(target_mineral)
        credit_amount = credit_amount - target_mineral.market_price

    prob_array, ubound = generate_prob_array(credit_amount, target_mineral)

    if len(prob_array) > 0:
        roll = Decimal(random.uniform(0, float(ubound-1)))

        mineral = get_roll_result(roll, prob_array)
        credit_amount = credit_amount - mineral.market_price
        result.append(mineral)

        if Mineral.objects.filter(market_price__lt=credit_amount).count() > 0:
            result = result + generate_pole_results(credit_amount, target_mineral, False)

    return result
