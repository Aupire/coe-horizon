import hashlib
import random
import string

from django.db.models.signals import post_save
from django.dispatch import receiver

from geology.models import Pole


@receiver(post_save, sender=Pole)
def ensure_pole_has_security_token(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.security_token is None or instance.security_token == '':
        token_salt = ''
        r = [random.choices(string.ascii_lowercase + string.digits) for i in range(20)]
        for i in r:
            token_salt += i[0]

        m = hashlib.sha256()
        m.update(token_salt.encode())

        instance.security_token = m.hexdigest()
        instance.save()
