from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('account_overview/', views.account_overview, name='account_overview'),
    path('request_payment/', views.request_payment, name='request_payment'),
    path('provide_payment/', views.provide_payment, name='provide_payment'),
    path('confirm_payment/', views.confirm_payment, name='confirm_payment'),
    path('confirm_payment/<int:account_id>/<int:amount_paid>', views.confirm_payment, name='confirm_payment_with_args'),
    path('process_payment/', views.process_payment, name='process_payment'),
    path('use_corruption/', views.use_corruption, name='use_corruption'),
    path('mechanic_skill/', views.mechanic_skill, name='mechanic_skill'),
    path('display_corruption_cost/', views.display_corruption_cost, name='display_corruption_cost'),
]
