import json

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import redirect, render
from django.urls import reverse

from bank_account.models import BankAccount, Transaction
from character import views as char_views
from character.models import Character
from character.utils import get_character, get_generic_account, char_has_skill

from downtime.utils import get_corruption_cost

from .forms import ConfirmPaymentForm, RequestPaymentForm, DirectPaymentForm

# Create your views here.
@login_required
def mechanic_skill(request):
    character = get_character(request)
    context = {}
    context['character'] = character
    if character.is_locked or not char_has_skill(character, 'Mécanique'):
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        form = DirectPaymentForm(request.POST)
        if form.is_valid():
            transaction = form.save(commit=False)
            transaction.source=(character.bank_account)
            transaction.destination=(get_generic_account('Compétence: Méchanique').bank_account)

            try:
                transaction.save()
            except Exception as e:
                context['error'] = e.__str__()
                account = character.bank_account
                context['transactions'] = Transaction.objects.filter(Q(source=account) | Q(destination=account)).order_by('-datetime')
                context['character'] = character
                return render(request, 'bank_account/overview.djhtml', context)

            return redirect(account_overview)

    context['form'] = DirectPaymentForm()

    return render(request, 'bank_account/use_mechanic.djhtml', context)

@login_required
def use_corruption(request):
    character = get_character(request)
    if character.is_locked or not char_has_skill(character, 'Corruption'):
        return redirect(char_views.account_compromised)

    context = {}
    corruption_cost = get_corruption_cost(character)
    corruption_account = get_generic_account('Compétence: Corruption')

    from_account = character.bank_account
    to_account = corruption_account.bank_account

    amount = corruption_cost

    transaction = Transaction(amount=corruption_cost, source=from_account, destination=to_account)
    try:
        transaction.save()
    except Exception as e:
        context['error'] = e.__str__()
        account = character.bank_account
        context['transactions'] = Transaction.objects.filter(Q(source=account) | Q(destination=account)).order_by('-datetime')
        context['character'] = character
        return render(request, 'bank_account/overview.djhtml', context)

    character.current_influence += 1
    character.save()
    
    return redirect(account_overview)

@login_required
def display_corruption_cost(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context = {}
    context['character'] = character
    context['corruption_cost'] = get_corruption_cost(character)

    return render(request, 'bank_account/display_corruption_cost.djhtml', context)

@login_required
def provide_payment(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)
    context = {}

    context['form'] = ConfirmPaymentForm()
    context['character'] = character

    return render(request, 'bank_account/provide_payment.djhtml', context)

@login_required
def process_payment(request):
    context = {}
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        form = ConfirmPaymentForm(request.POST)
        if form.is_valid():
            account_id = form.cleaned_data['account_id']
            amount = form.cleaned_data['amount_to_provide']
            account_to = BankAccount.objects.get(pk=account_id)
            account_from = character.bank_account

            transaction = Transaction(amount=amount, source=account_from, destination=account_to)
            try:
                transaction.save()
            except Exception as e:
                context['error'] = e.__str__()
                account = character.bank_account
                context['transactions'] = Transaction.objects.filter(Q(source=account) | Q(destination=account)).order_by('-datetime')
                context['character'] = character
                return render(request, 'bank_account/overview.djhtml', context)

            return redirect(account_overview)

    return render(request, 'bank_account/confirm_payment.djhtml', context)


@login_required
def confirm_payment(request, account_id=None, amount_paid=None):
    context = {}
    character = get_character(request)
    context['character'] = character
    if character.is_locked:
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        form = ConfirmPaymentForm(request.POST)
        if form.is_valid():
            account_id = form.cleaned_data['account_id']
            context['dest_amount'] = form.cleaned_data['amount_to_provide']

            account = BankAccount.objects.get(pk=account_id)
            context['dest_char'] = Character.objects.filter(bank_account=account).first()
            context['account_id'] = account_id
            context['form'] = form

    if account_id is not None and amount_paid is not None:
        context['dest_amount'] = amount_paid
        account = BankAccount.objects.get(pk=account_id)
        context['dest_char'] = Character.objects.filter(bank_account=account).first()
        context['account_id'] = account_id

        context['form'] = ConfirmPaymentForm(initial={
            'account_id': account_id,
            'amount_to_provide': amount_paid,
        })

    return render(request, 'bank_account/confirm_payment.djhtml', context)

@login_required
def request_payment(request, amount=None):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)
    context = {}

    if request.method == 'POST':
        form = RequestPaymentForm(request.POST)
        if form.is_valid():
            amount = form.cleaned_data['amount']

            context['qr'] = request.build_absolute_uri(
                reverse('confirm_payment_with_args',
                        kwargs={
                            'account_id': character.bank_account.id,
                            'amount_paid': amount,
                        }))


    context['form'] = RequestPaymentForm()
    context['character'] = character
    return render(request, 'bank_account/request_payment.djhtml', context)


@login_required
def account_overview(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    account = character.bank_account
    transactions = Transaction.objects.filter(Q(source=account) | Q(destination=account)).order_by('-datetime')

    return render(request, 'bank_account/overview.djhtml', {
        'character': character,
        'transactions': transactions,
    })
