import random
from datetime import datetime, time, timedelta
from decimal import Decimal

from django.shortcuts import redirect, render

from character import views as char_views
from character.models import Character, Skill, SkillBought
from character.utils import char_has_skill, get_character, get_generic_account
from character.views import character_sheet, view_login
from extra_mail.models import ContactList, Message, MessageStatus
from extra_mail.views import view_message
from hacking.forms import HackTargetForm, HackChooseExtraRisk
from hacking.models import HackAttempt, HackProgress
from hacking.util import prepare_hack, get_stealable_mails
from hacking.models import HackAttempt, HackProgress
from hacking.exceptions import AccountCompromised, HackedToday
from program.models import InstalledProgram, ProgramType

# Create your views here.

def swipe_overview(request):
    character = get_character(request)
    contacts = ContactList.objects.get(owner=character)
    context = {}
    if character.is_locked:
        return redirect(char_views.account_compromised)
    if char_has_skill(character, 'Piratage simple'):
        context['character'] = character
        context['contact_list'] = contacts
        
        return render(request, 'hacking/swipe_overview.djhtml', context)       
    else:
        return redirect(view_login)

def swipe_target(request, target_id):
    character = get_character(request)
    contacts = ContactList.objects.get(owner=character)
    target = Character.objects.get(id=target_id)
    if character.is_locked:
        return redirect(char_views.account_compromised)
    if char_has_skill(character, 'Piratage simple') and target in contacts.contacts.all():
        try:
            prepare_hack(
                character,
                target,
                detection_chance=0.05,
                hacked_amount=0.1
                )
            contacts.contacts.remove(target)
            contacts.save()
        except HackedToday:
            return redirect(hack_overview, compromised=False, cooldown=True)
    else:
        # Funny stuff is happening, redirect to login to be safe
        return redirect(view_login)

    return redirect(hack_attempts)

def stealable_emails(request):
    context = {}
    character = get_character(request)
    context['character'] = character

    stealable_mail = get_stealable_mails(context['character'])
    context['mails'] = stealable_mail

    return render(request, 'hacking/stealable_mail.djhtml', context)       


def steal_email(request, message_id, attempt_id):
    context = {}
    character = get_character(request)

    mail = Message.objects.get(id=message_id)
    attempt = HackAttempt.objects.get(id=attempt_id)
    stealable_mail = get_stealable_mails(character)

    mails = []
    for i in stealable_mail:
        mails += i['mails']

    if mail in mails:
        pirated_mail = Message(
            by=get_generic_account('Piratage'),
            subject=f'Piratage: {mail.subject}',
            content='Courriel piraté avec succès',
            previous=mail
        )
        pirated_mail.save()
        pirated_mail.to.add(character)
        pirated_mail.save()
        message_status = MessageStatus.objects.get(owner=character, message=pirated_mail)

        attempt.stolen_mails.add(mail)
        if attempt.stolen_mails.count() >= attempt.email_to_steal:
            attempt.processed = True
            
        attempt.save()

        return redirect(view_message, message_status.id)
    else:
       return redirect(hack_overview)


def hack_target_mail(request, target_id):
    context = {}
    context['mail'] = True
    context['target_id'] = target_id

    character = get_character(request)
    progress = HackProgress.objects.filter(hacker=character)
    target = Character.objects.get(id=target_id)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        form = HackChooseExtraRisk(request.POST)
        if form.is_valid():
            try:
                target_progress = progress.filter(target=target, processed=False).first()
                
                if target_progress and char_has_skill(character, 'Piratage simple'):
                   hacked_amount = 4
                   detection_chance = 0.05 
                        
                   if char_has_skill(character, 'Piratage opaque'):
                       detection_chance = 0.1

                   detection_chance += form.cleaned_data['extra_stolen_things'] * 0.05
                   hacked_amount += form.cleaned_data['extra_stolen_things']
                   attempt = prepare_hack(
                       character,
                       target,
                       detection_chance=detection_chance,
                       hacked_amount = hacked_amount,
                       stealing='emails'
                   )

                   target_progress.processed = True
                   target_progress.hack_attempt = attempt
                   target_progress.save()

                   return redirect(hack_attempts)
            except HackedToday:
                return redirect(hack_overview, compromised=False, cooldown=True)
        else:
            return redirect(hack_overview)
    else:
        context['form'] = HackChooseExtraRisk()
        context['form'].fields['extra_stolen_things'].label = "Nombre de courriels visé supplémentaires"
    
    return render(request, 'hacking/choose_risk.djhtml', context)
            
def hack_target_credits(request, target_id):
    context = {}
    context['credits'] = True
    context['target_id'] = target_id

    character = get_character(request)
    progress = HackProgress.objects.filter(hacker=character)
    target = Character.objects.get(id=target_id)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        form = HackChooseExtraRisk(request.POST)
        if form.is_valid():
            try:
                target_progress = progress.filter(target=target, processed=False).first()
                
                if target_progress and char_has_skill(character, 'Piratage simple'):
                    try:
                        hacked_amount = 0.2 
                        detection_chance = 0.05 
                        
                        if char_has_skill(character, 'Piratage opaque'):
                            hacked_amount = 0.3
                            detection_chance = 0.1

                        detection_chance += form.cleaned_data['extra_stolen_things'] * 0.02
                        hacked_amount += form.cleaned_data['extra_stolen_things'] * 0.01
                        attempt = prepare_hack(
                            character,
                            target,
                            detection_chance=detection_chance,
                            hacked_amount=hacked_amount
                        )

                        target_progress.processed = True
                        target_progress.hack_attempt = attempt
                        target_progress.save()
                            
                        return redirect(hack_attempts)
                    except HackedToday:
                        return redirect(hack_overview, compromised=False, cooldown=True)
                    else:
                        return redirect(hack_overview)
            except:
                pass
    else:
        context['form'] = HackChooseExtraRisk()
        context['form'].fields['extra_stolen_things'].label = "% de crédit piraté visé supplémentaires"

    return render(request, 'hacking/choose_risk.djhtml', context)

def hack_attempts(request):
    context = {}
    context['character'] = get_character(request)
    if context['character'].is_locked:
        return redirect(char_views.account_compromised)
    context['attempts'] = HackAttempt.objects.filter(hacker=context['character']).order_by('-id')

    return render(request, 'hacking/attempts.djhtml', context)       

def hack_overview(request, compromised=False, cooldown=False):
    context = {}

    if compromised == 'True': context['error_compromised'] = compromised
    if cooldown == 'True': context['error_cooldown'] = cooldown
    
    context['character'] = get_character(request)
    if char_has_skill(context['character'], "Cryptographie"):
        context['crypto'] = True
    if context['character'].is_locked:
        return redirect(char_views.account_compromised)
    context['attempts'] = HackProgress.objects.filter(hacker=context['character'], processed=False).order_by('-id')

    return render(request, 'hacking/overview.djhtml', context)
