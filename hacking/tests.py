from django.test import TestCase
from hacking.models import HackAttempt, HackProgress
from hacking.util import prepare_hack
from character.models import Character, SkillBought, Skill
from extra_mail.models import Message, MessageStatus
from bank_account.models import Transaction
from program.models import ProgramType, InstalledProgram
from character.management.commands.parse import run as parserun
from hacking.exceptions import HackedToday

from character.utils import get_generic_account

# Create your tests here.
class HackingTestCase(TestCase):
    def setUp(self):
        self.hacker = get_generic_account('hacker')
        self.target = get_generic_account('target')
        hacking_skill = Skill(name='Piratage simple', xp_cost=1, description='')
        hacking_skill.save()

        sb = SkillBought(character=self.hacker,
                         skill=Skill.objects.get(name='Piratage simple'))
        sb.save()

    def test_hackprogress_sent_credits(self):
        self.hacker.bank_account.balance=1000
        self.hacker.bank_account.save()

        self.assertEqual(
            HackProgress.objects.filter(hacker=self.hacker, target=self.target).count(),
            0
        )

        transaction = Transaction(amount=50,
                    source=self.hacker.bank_account,
                    destination=self.target.bank_account)
        transaction.save()

        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).sent_creds)

    def test_hackprogress_received_credits(self):
        self.target.bank_account.balance=1000
        self.target.bank_account.save()

        self.assertEqual(
            HackProgress.objects.filter(hacker=self.hacker, target=self.target).count(),
            0
        )

        transaction = Transaction(amount=50,
                    destination=self.hacker.bank_account,
                    source=self.target.bank_account)
        transaction.save()

        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).received_creds)

    def test_hackprogress_started_convo(self):
        self.assertEqual(
            HackProgress.objects.filter(hacker=self.hacker, target=self.target).count(),
            0
        )

        message = Message(by=self.hacker,
                          subject='',
                          content='')
        message.save()
        message.to.add(self.target)
        message.save()

        self.assertFalse(HackProgress.objects.get(hacker=self.hacker, target=self.target).started_convo)

        status = MessageStatus.objects.get(message=message, owner=self.target)
        status.read=True
        status.save(update_fields=['read'])

        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).started_convo)



    def test_hackprogress_talked_to(self):
        self.assertEqual(
            HackProgress.objects.filter(hacker=self.hacker, target=self.target).count(),
            0
        )

        message = Message(by=self.target,
                          subject='',
                          content='')
        message.save()
        message.to.add(self.hacker)
        message.save()

        message2 = Message(by=self.hacker,
                           subject='',
                           content='',
                           previous=message)

        message2.save()
        message2.to.add(self.target)
        message2.save()

        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).talked_to)

    def test_hackprogress_ongoing_pm(self):
        prev_message=None
        sender_list=[self.hacker, self.target]


        for isent in range(1,7):
            isender = isent % 2
            if isender == 1:
                ito = 0
            else:
                ito = 1

            message = Message(by=sender_list[isender],
                              subject='',
                              content='',
                              previous=prev_message)
            message.save()
            message.to.add(sender_list[ito])
            message.save()
            prev_message=message

            if isent < 5:
                self.assertFalse(HackProgress.objects.get(hacker=self.hacker, target=self.target).ongoing_pm)
                
        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).ongoing_pm)


    def test_hackprogress_installed_software(self):
        self.assertEqual(
            HackProgress.objects.filter(hacker=self.hacker, target=self.target).count(),
            0
        )

        programtype = ProgramType.objects.all().first()
        ip = InstalledProgram(target=self.target, installed_by=self.hacker, program=programtype)
        ip.save()

        self.assertTrue(HackProgress.objects.get(hacker=self.hacker, target=self.target).installed_software)


    def test_credit_hack_works(self):
        target_account = self.target.bank_account
        hacker_account = self.hacker.bank_account

        target_account.balance = 100
        target_account.save()

        self.assertEqual(target_account.balance, 100)
        self.assertEqual(hacker_account.balance, 0)

        attempt = prepare_hack(self.hacker,
                     self.target,
                     detection_chance=0,
                     hacked_amount=0.1)

        self.assertEqual(target_account.balance, 90)
        self.assertEqual(hacker_account.balance, 10)

    def test_second_hack_requires_vc_program(self):
        attempt = prepare_hack(self.hacker,
                               self.target,
                               detection_chance=0,
                               hacked_amount=1)

        self.assertRaises(HackedToday, prepare_hack,
                          hacker=self.hacker,
                          target=self.target,
                          detection_chance=0,
                          hacked_amount=1)

        
        
        vc = ProgramType.objects.get(id_code='VC')
        installed = InstalledProgram(target=self.hacker,
                                     installed_by=self.hacker,
                                     program=vc)
        installed.save()

        attempt = prepare_hack(self.hacker,
                               self.target,
                               detection_chance=0,
                               hacked_amount=1)

        # VC should make it +5% risk per attempt
        self.assertEqual(attempt.detection_chance, 0.05)

        vc = ProgramType.objects.get(id_code='VC')
        installed = InstalledProgram(target=self.hacker,
                                     installed_by=self.hacker,
                                     program=vc)
        installed.save()

        attempt = prepare_hack(self.hacker,
                               self.target,
                               detection_chance=0,
                               hacked_amount=1)

        # VC should make it +5% risk per attempt
        self.assertEqual(attempt.detection_chance, 0.1)
