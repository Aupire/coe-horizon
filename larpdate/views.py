from django.shortcuts import render, redirect
from django.http import JsonResponse
from character.utils import get_character, select_character
from larpdate.forms import GetUrlForm
from larpdate.utils import get_link_corp_and_date
from larpdate.models import LarpDate, ScannedTicket


# Create your views here.
def scan_ticket(request):
    context = {}
    character = get_character(request)
    if character.is_alterego:
        select_character(character.parent_char)
        character = character.parent_char

    context['form'] = GetUrlForm()
    context['character'] = character

    if request.method == 'POST':
        url = request.POST['url'].replace("https://www.zeffy.com/ticket/", "")
        return redirect('view_ticket_scan_result', url=url)

    return render(request, 'larpdate/scan_ticket.djhtml', context)


def view_ticket_scan_result(request, url):
    character = get_character(request)
    context = {
        'character': character,
        'url': url,
        'skip_ticket_warning': True
    }

    return render(request, 'larpdate/ticket_claim.djhtml', context)


def check_ticket(request, url):
    context = {}
    character = get_character(request)

    if len(ScannedTicket.objects.filter(ticket_url="https://www.zeffy.com/ticket/" + url)) > 0:
        context['error'] = 'Ce billet à déjà été utilisé!'
        return JsonResponse(context)

    values = get_link_corp_and_date(url)

    if character.corporation != values['corp']:
        context['error'] = 'La corporation de votre personnage ne correspond pas à ce billet'

        return JsonResponse(context)

    if values['season']:
        larps = LarpDate.objects.filter(out_date__year=values['date'])
    else:
        larps = LarpDate.objects.filter(out_date=values['date'])

    for larp in larps:
        character.paid_larps.add(larp)
    character.save()

    ticket = ScannedTicket(ticket_url="https://www.zeffy.com/ticket/" + url)
    ticket.character = character
    ticket.save()

    return JsonResponse({'success': True})
