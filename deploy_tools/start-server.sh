#!/usr/bin/env bash
# start-server.sh
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (python manage.py createsuperuser --no-input)
fi

python manage.py migrate

(gunicorn horizon.wsgi:application --bind 0.0.0.0:8010) &
nginx -g "daemon off;"
