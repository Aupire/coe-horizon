zero_downtime_deploy() {  
  service_name=extranet


  docker-compose -f /srv/docker-compose.yaml up -d --no-deps --scale $service_name=1 --no-recreate $service_name
  old_container_id=$(docker ps -f name=$service_name -q | tail -n1)

  # bring a new container online, running new code  
  docker-compose -f /srv/docker-compose.yaml up -d --no-deps --scale $service_name=2 --no-recreate $service_name

  # wait for new container to be available  
  new_container_id=$(docker ps -f name=$service_name -q | head -n1)
  new_container_ip=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $new_container_id)
  curl --silent --include --retry-connrefused --retry 100 --retry-delay 10 --fail http://$new_container_ip:8020/ || exit 1 >> /dev/null


  # take the old container offline  
  docker stop $old_container_id
  docker rm $old_container_id

  docker-compose -f /srv/docker-compose.yaml up -d --no-deps --scale $service_name=1 --no-recreate $service_name
}

zero_downtime_deploy
