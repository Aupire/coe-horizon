from datetime import datetime
from bootstrap_datepicker_plus.widgets import DateTimePickerInput

from django.shortcuts import redirect, render
from django.urls import reverse

from anim_tools.forms import (BankTransferForm, CreateGenericAccountForm,
                              SelectCharacterForm, SelectPoleForm,
                              SendMailForm, InstalledProgramForm, AppliedConditionForm)
from geology.forms import InstallPoleForm
from geology.models import Pole
from character import views as char_views
from character.models import Character, Corporation, Origin, SkillBought
from character.utils import get_character, get_generic_account
from downtime.utils import get_corruption_cost
from extra_mail.models import Message
from extra_mail.utils import inbox_for_char
from bank_account.models import Transaction

# Create your views here.

# ---------------- MAIL ----------------
# TODO: Send mail at specific time

def anim_mail(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = SendMailForm(request.POST)
        if form.is_valid():
            message = Message(
                by = form.cleaned_data['sender'],
                subject = form.cleaned_data['subject'],
                content = form.cleaned_data['content']
            )

            if form.cleaned_data['timestamp'] is not None:
                message.timestamp = form.cleaned_data['timestamp']
            message.save()

            use_cci = form.cleaned_data['use_cci']

            if form.cleaned_data['send_to_all']:
                for character in Character.objects.filter(player__is_staff=False, player__isnull=False):
                    message.cci.add(character) if use_cci else message.to.add(character)

            if form.cleaned_data['send_to_specific']:
                for character in form.cleaned_data['send_to_specific']:
                    message.cci.add(character) if use_cci else message.to.add(character)

            corporations = {
                'Chrysalide Ltd.': 'send_to_chrysalide',
	            'Kanopi Alliance': 'send_to_kanopi',
	            'Nephilim Trust': 'send_to_nephilim',
	            'Goodman Industries': 'send_to_goodman',
	            'APEX Solutions': 'send_to_apex',
	            'Projet Ascension': 'send_to_ascension',
	            'Groupe Insta-Vie': 'send_to_instavie',
	            'Atlas Fondation': 'send_to_atlas',
	            'Alpha-Solaris Initiative': 'send_to_alpha',
            }

            for corp, field in corporations.items():
                if form.cleaned_data[field]:
                    for character in Character.objects.filter(corporation__name=corp, player__is_staff=False):
                        message.cci.add(character) if use_cci else message.to.add(character)


            skill_to_send_message = form.cleaned_data['send_to_skill_holder']
            skill_bought = SkillBought.objects.filter(character__player__is_staff=False, skill__in=skill_to_send_message)
            for sb in skill_bought:
                message.cci.add(sb.character) if use_cci else message.to.add(sb.character)
            
            message.save()
            return redirect(anim_overview)


    form = SendMailForm()
    form.fields['timestamp'].widget = DateTimePickerInput()
    context = {
        'form': form,
        'next': 'anim_mail',
        'selecting': 'message',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def apply_condition(request, from_id=None):
    context = {}

    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = AppliedConditionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(anim_overview)

    context = {
        'form': AppliedConditionForm(),
        'next': 'apply_condition',
        'selecting': 'Ajout de condition',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def report_corruption(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    corps = Corporation.objects.filter(anim_only=False)
    prices = {}
    for c in corps:
        prices[c] = get_corruption_cost(
            Character.objects.filter(corporation=c).first()
        )

    context = {
        'prices': prices,
        'selecting': 'Corruption Prices'
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def install_program(request, from_id=None):
    context = {}

    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = InstalledProgramForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(anim_overview)

    context = {
        'form': InstalledProgramForm(),
        'next': 'install_program',
        'selecting': 'Installation de programme',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def anim_bank_transfer(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = BankTransferForm(request.POST)
        if form.is_valid():
            transfer = form.save(commit=False)

            character = transfer.source.character
            if character.player is None or character.player.is_staff:
                source = transfer.source
                source.balance = source.balance + transfer.amount
                source.save()

            transfer.save()

            return redirect(anim_overview)

    context = {
        'form': BankTransferForm(),
        'next': 'anim_bank_transfer',
        'selecting': 'Transfert Banquaire',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


# --------------- ACCOUNT ----------------

def anim_create_generic_account(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = CreateGenericAccountForm(request.POST)
        if form.is_valid():
            account_name = form.cleaned_data['account_name']

            character = Character(
                origin = Origin.objects.first(),
                corporation = Corporation.objects.first(),
                first_name = account_name,
            )

            character.save()

            return redirect(anim_overview)

    context = {
        'form': CreateGenericAccountForm(),
        'next': 'anim_create_generic_account',
        'selecting': 'Compte Générique',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def anim_overview(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    context = {}
    context['user'] = user

    return render(request, 'anim_tools/overview.djhtml', context)

def all_player_cards(request, pending_print=False, anim_print=False):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    characters = []
    if pending_print == "True":
        if not anim_print == "True":
            characters = Character.objects.filter(pending_print=True).filter(player__is_staff=False)
        else:
            characters = Character.objects.filter(pending_print=True).filter(player__is_staff=True)
    else:
        characters = Character.objects.filter(player__is_staff=False)

    items = []
    for character in characters:
        if character.pending_print:
            character.pending_print = False
            character.save()

        qr = request.build_absolute_uri(
            reverse('add_contact_with_args',
                kwargs={
                    'char_id': character.id,
                    'security_token': character.security_token,
                }))

        
        items.append({
            'qr': qr,
            'character': character,
            'corporation_logo': 'img/' + character.corporation.__str__().lower() + '.svg'
        })

    context = {'items': items }

    return render(request, 'anim_tools/access_card.djhtml', context)

def specific_player_card(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = SelectCharacterForm(request.POST)
        if form.is_valid():
            character = form.cleaned_data['character']


            qr = request.build_absolute_uri(
                reverse('add_contact_with_args',
                kwargs={
                    'char_id': character.id,
                    'security_token': character.security_token,
                }))

            context = {
                'items': [{
                    'qr': qr,
                    'character': character,
                    'corporation_logo': 'img/' + character.corporation.__str__().lower() + '.svg',
                }]
            }

            return render(request, 'anim_tools/access_card.djhtml', context)

    context = {
        'form': SelectCharacterForm(),
        'next': 'specific_player_card',
        'selecting': 'personage',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)

def view_emails_of(request):
    user = request.user
    if not user.is_staff:
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = SelectCharacterForm(request.POST)
        if form.is_valid():
            character = form.cleaned_data['character']
            context = {
                'character': character,
            }

            return inbox_for_char(request, character, character.id)

    context = {
        'form': SelectCharacterForm(),
        'next': 'view_char_emails',
        'selecting': 'personage',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)


def install_pole(request):
    user = request.user
    context = { 'user': user }
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = InstallPoleForm(request.POST)
        if form.is_valid():
            geology = get_generic_account('Compétence: Géologie')

            pole = form.save(commit=False)

            transaction = Transaction(
                source=pole.owner.bank_account,
                destination=geology.bank_account,
                amount=50
            )

            try:
                transaction.save()
            except Exception as e:
                context['error'] = e.__str__()
                return render(request, 'anim_tools/overview.djhtml', context)

            pole.installed_at = datetime.now()
            pole.started = True
            pole.save()
            return redirect(anim_overview)
        return redirect(install_pole)

    context = {
        'form': InstallPoleForm(),
        'next': 'install_pole',
        'selecting': 'Envoie de Drone'
    }

    return render(request, 'anim_tools/select_character.djhtml', context)

def list_poles(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    poles = Pole.objects.filter(completed=False)

    context = {
        'poles': poles
    }

    return render(request, 'anim_tools/list_poles.djhtml', context)


def collect_pole(request, pole_id):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    pole = Pole.objects.get(id=pole_id)
    pole.collect()

    context = {
        'output': pole.output
    }

    return render(request, 'anim_tools/display_results.djhtml', context)


def get_pole_qr(request):
    user = request.user
    if not user.is_staff:
        # fishy stuff happening!
        return redirect(char_views.view_login)

    if request.method == 'POST':
        form = SelectPoleForm(request.POST)
        if form.is_valid():
            pole = form.cleaned_data['pole']

            qr = {
                'security_token': pole.security_token,
                'pole_id': pole.id,
            }
            context = {
                'qr': qr,
                'pole_id': pole.id,
            }

            return render(request, 'geology/pole_qr.djhtml', context)

    context = {
        'form': SelectPoleForm(),
        'next': 'get_pole_qr',
        'selecting': 'pole géologique',
    }

    return render(request, 'anim_tools/select_character.djhtml', context)
