from datetime import datetime, timedelta

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from biomass.models import Alert
from biomass.utils import get_next_run
from larpdate.models import LarpDate
from larpdate.utils import get_previous_larp

from datetime import timedelta


@receiver(post_save, sender=Alert)
def ensure_alert_has_execute_time(sender, instance, created, raw, using, update_fields, **kwargs):
    now = datetime.now()
    larpdate = get_previous_larp()
    

    if instance.execute_at is None:
        # First initialization
        target_hours = instance.get_target_hours()
        target_datetime = now + timedelta(hours=target_hours)
        execute_at = get_next_run(target_datetime)

        instance.execute_at = execute_at

        exec_at = instance.execute_at
        cutoff = larpdate.end_datetime - timedelta(hours=2)
        if exec_at.tzinfo is None:
            exec_at = timezone.make_aware(exec_at)
        if cutoff.tzinfo is None:
            cutoff = timezone.make_aware(cutoff)

        if instance.execute_at.hour < 9:
            instance.execute_at = instance.execute_at.replace(hour=9)
            instance.expires_at = instance.execute_at.replace(hour=12)

        # Only execute if alert is within LARP cutoff time
        if exec_at < cutoff:
           instance.save()
        else:
            instance.execute_at = None

     # Ensure expiration is 3 hours after execute_at
    if instance.expires_at is None and instance.execute_at is not None:
        instance.expires_at = instance.execute_at + timedelta(hours=3)
        instance.save()


