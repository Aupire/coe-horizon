from django.shortcuts import redirect, render

from character import views as char_views
from character.models import Skill
from character.utils import char_has_skill, get_character
from innovation.models import Innovation, Source


# Create your views here.
def innovation_overview(request):
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    innovations = Innovation.objects.filter(owner=character)
    innovation_skills = Skill.objects.filter(name__icontains='innovation')
    innovation_skills = [s for s in innovation_skills if char_has_skill(character, s.name)]

    if len(innovation_skills) == 0:
        return redirect(char_views.view_login)

    context = {
        'character': character,
        'innovations': innovations,
        'innovation_skills': innovation_skills
    }

    return render(request, 'innovation/overview.djhtml', context)
