from django.contrib.auth import views as auth_views
from django.urls import path

from innovation import views

urlpatterns = [
    path('innovation_overview/', views.innovation_overview, name='innovation_overview'),
]
