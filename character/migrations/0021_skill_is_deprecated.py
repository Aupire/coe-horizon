# Generated by Django 2.2.4 on 2023-01-05 22:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('character', '0020_skill_has_contact_skill'),
    ]

    operations = [
        migrations.AddField(
            model_name='skill',
            name='is_deprecated',
            field=models.BooleanField(default=False),
        ),
    ]
