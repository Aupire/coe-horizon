# Generated by Django 2.0 on 2019-05-23 22:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('character', '0006_character_bank_account'),
    ]

    operations = [
        migrations.AlterField(
            model_name='character',
            name='bank_account',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='bank_account.BankAccount'),
        ),
    ]
