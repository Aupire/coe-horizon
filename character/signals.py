import hashlib
import random
import string

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from bank_account.models import BankAccount
from character.models import Character, Grade, Skill, SkillBought, Origin, Corporation
from extra_mail.models import ContactList, Message
from character.utils import get_generic_account, pay_character, get_skillbought_effective_cost

@receiver(pre_save, sender=SkillBought)
def ensure_skillbought_cost_overrides(sender, instance, **kwargs):
  if get_skillbought_effective_cost(instance) == 0:
    instance.cost_override = 0


@receiver(post_save, sender=SkillBought)
def ensure_contact_skill_is_added(sender, instance, created, raw, using, update_fields, **kwargs):
  if instance.skill.has_contact_skill:
      contact_list = ContactList.objects.all().filter(owner=instance.character).first()
      contact = Character.objects.all().filter(first_name=instance.skill.contact_name).first()
      contact_list.contacts.add(contact)
      contact_list.save()

@receiver(post_save, sender=Skill)
def advise_players_of_deprecated_skills(sender, instance, created, raw, using, update_fields, **kwargs):
  body = f"Cher joueur, veuillez noter que la compétence {instance.name} à du être retiré de votre fiche du à des changements de système. Il est probable qu'elle ai été remplacée par une compétence équivalente que nous n'ayons pas pu migré automatiquement. Veuillez racheter la compétence de remplacement normalement. Pour toutes questions, contactez l'équipe d'animation"
  subject = "[HORS JEU] Compétence retirée"
  admin = get_generic_account("HORS_JEU_ADMIN")
  if instance.is_deprecated:

    for sb in SkillBought.objects.all().filter(skill=instance):
      message = Message(
        by=admin,
        subject=subject,
        content=body
      )
      message.save()
      message.to.add(sb.character)
      message.save()
      sb.delete()

    instance.delete()


@receiver(post_save, sender=Skill)
def ensure_contact_skill_has_account(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.has_contact_skill:
      contact_name = instance.contact_name
      char = Character.objects.all().filter(first_name=contact_name, player=None)
      if char.count()==0:
          character = Character(
              origin=Origin.objects.first(),
              corporation=Corporation.objects.first(),
              first_name=contact_name,
          )
          character.save()


@receiver(post_save, sender=Character)
def ensure_character_has_account(sender, instance, created, raw, using, update_fields, **kwargs):
    if created or instance.bank_account is None:
        new_account = BankAccount()
        new_account.save()
        instance.bank_account = new_account
        instance.save()


@receiver(post_save, sender=Character)
def ensure_character_has_security_token(sender, instance, created, raw, using, update_fields, **kwargs):
    if created or instance.security_token is None:
        token_salt = ''
        r = [random.choices(string.ascii_lowercase + string.digits) for i in range(20)]
        for i in r:
            token_salt += i[0]

        m = hashlib.sha256()
        m.update(instance.full_name.encode())
        m.update(token_salt.encode())

        instance.security_token = m.hexdigest()
        instance.save()


@receiver(post_save, sender=Character)
def ensure_no_other_char_is_selected_on_creation(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        player = instance.player
        player_chars = Character.objects.all().filter(player=player)

        for char in player_chars:
            if char != instance:
                char.selected = False
                char.save()


@receiver(post_save, sender=Character)
def ensure_char_has_grade(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.grade is None:
        base_grade = Grade.objects.get(name='Employé')
        instance.grade = base_grade
        instance.save()


# @receiver(post_save, sender=Character)
# def ensure_active_character_is_paid(sender, instance, created, raw, using, update_fields, **kwargs):
#   if instance.old and instance.old.current_larp_active == False and instance.current_larp_active:
#     pay_character(instance)

@receiver(post_save, sender=Character)
def ensure_npc_has_valid_balance(sender, instance, created, raw, using, update_fields, **kwargs):
  instance.bank_account.save()

@receiver(post_save, sender=Character)
def ensure_active_character_is_paid(sender, instance, created, raw, using, update_fields, **kwargs):
  if instance.old and instance.old.grade != instance.grade:
    instance.pending_print = True
    instance.save()


@receiver(pre_save, sender=Character)
def pre_update_model(sender, **kwargs):
    # check if the updated fields exist and if you're not creating a new object
    if not kwargs['update_fields'] and kwargs['instance'].id:
        # Save it so it can be used in post_save
        kwargs['instance'].old = Character.objects.get(id=kwargs['instance'].id)

