from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.forms import ModelForm

from .models import Character, Skill, SkillBought


class CreateAccountForm(ModelForm):
    confirmation_du_mot_de_passe=forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
        labels = {
            'username': "Login",
            'first_name': "Prénom",
            'last_name': 'Nom de famille',
            'email': 'Addresse courriel',
            'password': 'Mot de passe',
        }
        widgets = {
            'password': forms.PasswordInput()
        }

    def __init__(self, *args, **kwargs):
        super(CreateAccountForm, self).__init__(*args, **kwargs)

        for f in ['username', 'password']:
            self.fields[f].help_text = None


    def clean(self):
        cleaned_data = super(CreateAccountForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirmation_du_mot_de_passe')

        if password != confirm_password:
            raise forms.ValidationError(
                'Le mot de passe ne corresponds pas'
            )

        username = cleaned_data.get('username')
        if User.objects.all().filter(username=username).exists():
            raise forms.ValidationError(
                "Ce nom d'utilisateur est déjà pris!"
            )

class CreateCharacterStaffForm(ModelForm):
    class Meta:
        model = Character
        fields = ['origin', 'corporation', 'grade', 'first_name', 'last_name', 'birth_place', 'age']
        labels = {
            'origin': 'Origine',
            'first_name': 'Prénom du personage',
            'last_name': 'Nom de famille du personage',
            'birth_place': 'Lieu de naissance',
            'age': 'Âge du personage',
            'grade': 'Grade',
        }


class CreateCharacterForm(ModelForm):
    class Meta:
        model = Character
        fields = ['origin', 'corporation', 'first_name', 'last_name', 'birth_place', 'age']
        labels = {
            'origin': 'Origine',
            'first_name': 'Prénom du personage',
            'last_name': 'Nom de famille du personage',
            'birth_place': 'Lieu de naissance',
            'age': 'Âge du personage',
        }

class BuySkill(ModelForm):
    skill = forms.ModelChoiceField(queryset=Skill.objects.order_by('name', 'xp_cost'))

    class Meta:
        model = SkillBought
        fields = ['skill']
        labels = {
            'skill': 'Compétence'
        }

class LoginForm(forms.Form):
    username = forms.CharField(label='Utilisateur', max_length=200)
    password = forms.CharField(label='Mot de Passe', widget=forms.PasswordInput)
    valuenext = forms.CharField(widget=forms.HiddenInput(), required=False)

class SelectActiveCharacter(forms.Form):
    character = forms.ModelChoiceField(queryset=None, empty_label=None, label='Personnage Actif')

    def __init__(self, *args, **kwargs):
        if 'user' in kwargs:
            user = kwargs.pop('user')

        super().__init__(*args, **kwargs)

        qs = Character.objects.filter(player=user, deactivated=False)
        self.fields['character'].queryset = qs
