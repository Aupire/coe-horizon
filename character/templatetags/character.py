from django import template

from character.utils import (char_has_innovation_skill, char_has_skill,
                             get_character)
from craft.models import RecipeCategory
from extra_mail.models import MessageStatus
from larpdate.utils import get_next_larp

from datetime import datetime

register = template.Library()


@register.simple_tag(takes_context=True)
def next_larp_is_paid(context):
    try:
        character = get_character(context['request'])
        if character.player.is_staff:
            return True

        if character.is_alterego:
            character = character.parent_char

        next_larp = get_next_larp()

        if next_larp is None:
            return True

        return next_larp in character.paid_larps.all()
    except Exception:
        return True


@register.simple_tag(takes_context=True)
def character_has_skill(context, skill):
    try:
        request = context['request']
        character = get_character(request)

        return char_has_skill(character, skill)
    except Exception:
        return ""


@register.simple_tag(takes_context=True)
def character_is_crafter(context):
    request = context['request']
    character = get_character(request)

    recipe_categories = RecipeCategory.objects.all()

    for category in recipe_categories:
        try:
            if char_has_skill(character, category.prerequisite_skill.name):
                return True
        except Exception:
            # Skipping empty categories
            pass

    return False


@register.simple_tag(takes_context=True)
def character_has_innovation_skill(context):
    request = context['request']
    character = get_character(request)

    return char_has_innovation_skill(character)


@register.simple_tag(takes_context=True)
def unread_messages_count(context):
    try:
        request = context['request']
        character = get_character(request)

        return MessageStatus.objects.filter(owner=character, archived=False, read=False, timestamp__lte=datetime.now()).count()
    except Exception:
        return 0


@register.filter
def multiply(value, arg):
    return value * arg
