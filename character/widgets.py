from .models import Skill
from django.forms import widgets

class SkillSelectorWidget(widgets.MultiWidget):
    def __init__(self, attrs=None):
        skills = [s.skill_name for s in Skill.objects.all()]
        _widgets = (
            widgets.Select(attrs=attrs, choices=skills),
            widgets.NumberInput,
        )
        super().__init(_widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.skill, value.count]
