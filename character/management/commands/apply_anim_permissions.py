from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Permission

def apply_permissions():
    models = [
        'installedprogram',
        'source',
        'innovation',
        'biomass',
        'appliedcondition',
        'condition',
        'character',
        'bankaccount',
    ]

    staff = User.objects.filter(is_staff=True)

    for model in models:
        permissions = Permission.objects.filter(content_type__model=model)
        for person in staff:
            person.user_permissions.add(*permissions)
        

class Command(BaseCommand):
    help = 'Provides Pay and start biomass timers when required'

    def handle(self, *args, **kwargs):
        apply_permissions()
