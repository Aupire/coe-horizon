from django.db.models import Q

from character.models import Character, Corporation, Origin, Skill, SkillBought, Grade
from bank_account.models import Transaction
from larpdate.models import LarpDate
from datetime import datetime
from craft.models import Recipe


def char_corp_is_ascendant(character):
    # Importing here to avoid circular imports
    from downtime.models import Sector
    corps = {
        'humanist': [
            Corporation.objects.get(name='Alpha-Solaris Initiative'),
            Corporation.objects.get(name='Atlas Fondation'),
            Corporation.objects.get(name='Groupe Insta-Vie'),
        ],
        'transhumanist': [
            Corporation.objects.get(name='Projet Ascension'),
            Corporation.objects.get(name='APEX Solutions'),
            Corporation.objects.get(name='Goodman Industries'),
        ],
        'evolutionists': [
            Corporation.objects.get(name='Nephilim Trust'),
            Corporation.objects.get(name='Kanopi Alliance'),
            Corporation.objects.get(name='Chrysalide Ltd.')
        ]
    }

    for k, v in corps.items():
        if character.corporation in v:
            percent_controlled = Sector.objects.filter(controlling_character__corporation__in=corps['humanist']).count() / Sector.objects.all().count()

            return percent_controlled >= 0.5

    return False


def get_paid_chars_last_larp():
    today = datetime.now().date()

    larp = LarpDate.objects.filter(out_date__lte=today).order_by('out_date').last()

    return Character.objects.filter(paid_larps__in=[larp])


def get_paid_chars_next_larp():
    today = datetime.now().date()

    larp = LarpDate.objects.filter(out_date__gte=today).order_by('out_date').first()

    return Character.objects.filter(paid_larps__in=[larp])


def select_character(character):
    # deactivate previously active character
    chars = Character.objects.filter(player=character.player, selected=True)
    for c in chars:
        c.selected = False
        c.save()

    # Select new character
    character.selected = True
    character.save()


def get_character(request):
    user = request.user
    return Character.objects.filter(Q(player=user) & Q(selected=True)).first()


def char_has_skill(character, skill_name):
    skill = Skill.objects.get(name=skill_name)

    return SkillBought.objects.filter(character=character, skill=skill).exists()


def char_has_skill_icontains(character, skill_prefix):
    return SkillBought.objects.filter(character=character, skill__name__icontains=skill_prefix).count()


def char_has_innovation_skill(character):
    return SkillBought.objects.filter(
        character=character,
        skill__name__icontains='innovation'
    ).exclude(skill__name__icontains='(').count() > 0


def get_generic_account(account_name):
    try:
        account = Character.objects.get(first_name=account_name, last_name='')
    except Exception:
        account = Character(
            first_name=account_name,
            last_name='',
            origin=Origin.objects.first(),
            corporation=Corporation.objects.first()
        )
        account.save()

    return account


def get_skillbought_effective_cost(skill_bought):
  origin = skill_bought.character.origin
  skill = skill_bought.skill

  # Terrien -> Migrant clandestin: Contact illicite
  if origin.name == 'Migrant clandestin' and 'Contact illicite' in skill.name:
    free_skills = SkillBought.objects.filter(character=skill_bought.character,
                                               skill__name__icontains='Contact illicite',
                                               cost_override=0)
    if free_skills.count() < 1:
      return 0

  # Bio-synthétique -> Augmentation
  # Conscience -> Augmentation
  # Androïdes - IA -> Augmentations*2
  if (
      origin.name == 'Bio-synthétique' or
      origin.name == 'Conscience' or
      origin.name == 'Androïde - IA'
  ) and "Acquisition d'augmentation" in skill.name:
    free_skills = SkillBought.objects.filter(character=skill_bought.character,
                                               skill__name__icontains="Acquisition d'augmentation",
                                               cost_override=0)
    free_count = 1
    if origin.name == 'Androïde - IA':
      free_count=2
    if free_skills.count() < free_count:
      return 0


  # Recycleur: Mécanique
  if origin.name == 'Recycleur' and 'Mécanique' == skill.name:
    free_skills = SkillBought.objects.filter(character=skill_bought.character,
                                               skill__name__icontains='Mécanique',
                                               cost_override=0)
    if free_skills.count() < 1:
      return 0


  # Martien -> Entrainement civil
  if origin.name == 'Martien' and 'Entrainement civil' == skill.name:
    free_skills = SkillBought.objects.filter(character=skill_bought.character,
                                               skill__name__icontains='Entrainement civil',
                                               cost_override=0)
    if free_skills.count() < 1:
      return 0

  return skill_bought.skill.xp_cost


def next_grade(grade):
    next_dict = {
        'Employé': Grade.objects.get(name='Senior'),
        'Senior': Grade.objects.get(name='Cadre'),
        'Cadre': Grade.objects.get(name='Coordinateur'),
    }

    try:
        return next_dict[grade.name]
    except Exception:
        return grade


def pay_character(character):
    from downtime.models import Sector
    # Get or create pay character
    pay = get_generic_account('Paye')
    grade = character.grade

    source_account = pay.bank_account
    source_account.balance = 9999999
    source_account.save()

    financement = Skill.objects.get(name='Financement')
    base_salary = grade.base_salary

    try:
        financement_bought = SkillBought.objects.filter(character=character, skill=financement)
        base_salary = base_salary + (financement_bought.count() * 500)
    except Exception:
        # Character has not bought Financement
        pass

    if char_corp_is_ascendant(character):
        base_salary = round(base_salary * 1.2)

    if Sector.objects.get(name="Finance").controlling_character == character:
        base_salary = round(base_salary * 1.2)

    if Sector.objects.get(name="Recherche & Développement").controlling_character == character:
        royalties = 0
        for recipe in Recipe.objects.filter(royalties_target=character):
            royalties += round(recipe.market_price * (recipe.royalties_percent / 100))

        base_salary += royalties

    if character.origin.name in ['Martien', "Dette d’embarquement"]:
        base_salary = round(base_salary * 0.8)

    pay_transaction = Transaction(amount=base_salary, source=source_account, destination=character.bank_account)
    pay_transaction.save()
