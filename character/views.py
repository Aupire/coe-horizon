from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse

from character.utils import get_character, char_has_skill, get_skillbought_effective_cost, select_character

from .forms import (BuySkill, CreateAccountForm, CreateCharacterForm,
                    CreateCharacterStaffForm,
                    LoginForm, SelectActiveCharacter)
from .models import Character, Corporation, SkillBought, Skill


# Create your views here.
def mark_to_print(request, character_id):
    character = Character.objects.get(id=character_id)

    character.pending_print = True
    character.save()

    return redirect('list_characters')


def access_card(request):
    character = get_character(request)

    qr = request.build_absolute_uri(
        reverse('add_contact_with_args',
                kwargs={
                    'char_id': character.id,
                    'security_token': character.security_token,
                })
    )

    context = {
        'items': [{
            'qr': qr,
            'character': character,
            'corporation_logo': 'img/' + character.corporation.__str__().lower() + '.svg'
        }],
    }

    return render(request, 'anim_tools/access_card.djhtml', context)


def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['confirmation_du_mot_de_passe'])
            user.save()
            return redirect('character_login')

    else:
        form = CreateAccountForm()

    return render(request, 'character/create_account.djhtml', {'form': form})

def view_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']


            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)

                valuenext = form.cleaned_data['valuenext']

                if valuenext != '':
                    return redirect(valuenext)
                else:
                    return redirect('list_characters')

            else:
                valuenext = form.cleaned_data['valuenext']
                return render(request, 'character/login.djhtml', {
                    'form': LoginForm(initial={'valuenext': valuenext}),
                    'error': "Incapable de se connecter avec les authentifiants fournis",
                })

    else:
        valuenext = request.GET.get('next')
        return render(request, 'character/login.djhtml', {
            'form': LoginForm(initial={'valuenext': valuenext}),
        })


@login_required(login_url='/character/login/')
def list_characters(request):
    user = request.user

    if request.method == 'POST':
        form = SelectActiveCharacter(request.POST, user=user)
        if form.is_valid():
            character = form.cleaned_data['character']
            select_character(character)

    available_characters = Character.objects.filter(player=user, deactivated=False)
    if available_characters.count() == 0:
        redirect('create_character', alter_ego_id=0)

    form = None
    alter_ego = Skill.objects.get(name='Alter-Ego Virtuel')
    alter_ego_count = SkillBought.objects.filter(character__player=user, character__deactivated=False, skill=alter_ego).count()

    alter_ego_id = 0
    if user.is_staff:
        can_create = True
        form = SelectActiveCharacter(user=user)
    elif alter_ego_count > 0:
        form = SelectActiveCharacter(user=user)
        if Character.objects.filter(player=user, character__deactivated=False).count() < 3:
            can_create = True
            char = get_character(request)
            if char:
                alter_ego_id = SkillBought.objects.filter(character__player=user, character__deactivated=False, skill=alter_ego).first().character.id
        else:
            can_create = False
    else:
        can_create = available_characters.count() == 0

    return render(request, 'character/list_chars.djhtml', {
        'can_create': can_create,
        'characters': available_characters,
        'staff': user.is_staff,
        'form': form,
        'alter_ego_id': alter_ego_id
    })


@login_required(login_url='/character/login/')
def create_character(request, alter_ego_id):
    if request.method == 'POST':
        if request.user.is_staff:
            form = CreateCharacterStaffForm(request.POST)
        else:
            form = CreateCharacterForm(request.POST)

        if form.is_valid():
            current_user = request.user
            character = form.save(commit=False)
            character.player = current_user
            if int(alter_ego_id) != 0:
                main_char = Character.objects.get(id=alter_ego_id)
                character.parent_char = main_char
                character.is_alterego = True

            character.save()
            character.bank_account.save()
            return redirect('sheet', character_id=character.id)

    if request.user.is_staff:
        form = CreateCharacterStaffForm()
    else:
        form = CreateCharacterForm()

    if not request.user.is_staff:
        form.fields['corporation'].queryset = Corporation.objects.filter(anim_only=False)

    return render(request, 'character/create_character.djhtml', {'form': form, 'alter_ego_id': alter_ego_id})


@login_required(login_url='/character/login/')
def character_sheet(request, character_id):
    character = get_object_or_404(Character, pk=character_id)

    if request.user != character.player and not request.user.is_staff:
        return redirect('character_login')

    return render(request, 'character/sheet.djhtml', {
        'character': character,
        'skill_boughts': SkillBought.objects.filter(character=character)
    })


@login_required(login_url='/character/login/')
def buy_skill(request, character_id):
    if request.method == 'POST':
        form = BuySkill(request.POST)
        if form.is_valid():
            current_character = Character.objects.get(pk=character_id)
            skill_bought = form.save(commit=False)
            skill_bought.character = current_character

            xp_cost = get_skillbought_effective_cost(skill_bought)
            if xp_cost > current_character.current_xp:
                return render(request, 'character/sheet.djhtml', {
                    'error': "Vous n'avez pas assez d'XP pour acheter cette compétence!",
                    'character': current_character,
                })

            current_user = request.user

            if current_user != current_character.player:
                return HttpResponse('Forbidden', status=403)


            skill_bought.save()
            return redirect('sheet', character_id=current_character.id)

    else:
        form = BuySkill()

    character = get_object_or_404(Character, pk=character_id)
    return render(request, 'character/sheet.djhtml', {
        'character': character,
        'form': form,
        'skill_boughts': SkillBought.objects.filter(character=character)
    })


def account_compromised(request):
    character = get_character(request)

    return render(request, 'character/compromised.djhtml', {'character': character})
