from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.Payment)
admin.site.register(models.Category)
admin.site.register(models.Committee)
admin.site.register(models.Event)
