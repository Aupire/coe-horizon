from django.shortcuts import render

from .models import ContactList, Message, MessageStatus

from datetime import datetime

def inbox_for_char(request, character, from_id=None, include_archive=False):
    context = {}

    if include_archive and include_archive != 'None':
        # url does funky things, if anything's specified, it's true.
        include_archive=True
    else:
        include_archive=False

    context['message_statuses'] = MessageStatus.objects.filter(
        owner=character,
        archived=include_archive,
        timestamp__lte=datetime.now()
    ).order_by('-timestamp')
    context['character'] = character
    context['inbox'] = True
    if from_id and from_id != 'None':
        context['from_id'] = from_id

    return render(request, 'extra_mail/overview.djhtml', context)

def outbox_for_char(request, character):
    context = {}

    context['message_statuses'] = MessageStatus.objects.filter(message__by=character).order_by('-timestamp')
    context['character'] = character
    context['outbox'] = True

    return render(request, 'extra_mail/overview.djhtml', context)
