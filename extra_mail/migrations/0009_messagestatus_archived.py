# Generated by Django 4.1.6 on 2024-01-02 15:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('extra_mail', '0008_auto_20230111_2009'),
    ]

    operations = [
        migrations.AddField(
            model_name='messagestatus',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
