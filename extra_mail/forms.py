from django import forms

from extra_mail.models import Message


class AddContactForm(forms.Form):
    char_id = forms.IntegerField(widget=forms.widgets.HiddenInput)
    security_token = forms.CharField(widget=forms.widgets.HiddenInput)

class SendMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['to', 'cci', 'subject', 'content']

    def __init__(self, *args, **kwargs):
        super(SendMessageForm, self).__init__(*args, **kwargs)

class ReplyMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['subject', 'content']

    def __init__(self, *args, **kwargs):
        super(ReplyMessageForm, self).__init__(*args, **kwargs)
