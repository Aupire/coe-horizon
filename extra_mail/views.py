import logging

from django.shortcuts import redirect, render
from django.urls import reverse

from character import views as char_views
from character.models import Character
from character.utils import get_character

from .forms import AddContactForm, ReplyMessageForm, SendMessageForm
from .models import ContactList, Message, MessageStatus
from .utils import inbox_for_char, outbox_for_char


# Create your views here.
def overview(request, from_id=None, archive=False):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character

    if character.is_locked:
        return redirect(char_views.account_compromised)

    return inbox_for_char(request, character, from_id, archive)

def display_qr(request):
    character = get_character(request)

    qr = request.build_absolute_uri(
        reverse('add_contact_with_args',
                kwargs={
                    'char_id': character.id,
                    'security_token': character.security_token,
                })
    )

    context = {
        'qr': qr,
        'character': character,
    }

    return render(request, 'extra_mail/display_qr.djhtml', context)

def outbox(request):
    character = get_character(request)

    if character.is_locked:
        return redirect(char_views.account_compromised)

    return outbox_for_char(request, character)

def contact_list(request):
    context = {}

    character = get_character(request)

    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['contact_list'] = ContactList.objects.filter(owner=character).first()
    context['character'] = character

    return render(request, 'extra_mail/contact_list.djhtml', context)

def mark_unread(request, message_status_id, from_id=None):
    context = {}

    character = get_character(request)
    if from_id == 'None':
        from_id = None

    message_status = MessageStatus.objects.get(id=message_status_id)
    if not request.user.is_staff and message_status.owner != character:
        return redirect(char_views.view_login)

    message_status.read = False
    message_status.save()
    
    return redirect(overview, from_id=from_id)

def mark_archived(request, message_status_id, from_id=None):
    context = {}

    character = get_character(request)
    if from_id == 'None':
        from_id = None

    message_status = MessageStatus.objects.get(id=message_status_id)
    if not request.user.is_staff and message_status.owner != character:
        return redirect(char_views.view_login)

    message_status.archived = True
    message_status.save()
    
    return redirect(overview, from_id=from_id)


def view_message(request, message_status_id, from_id=None):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character

    status = MessageStatus.objects.get(id=message_status_id)
    if status.owner != character and status.message.by != character and not request.user.is_staff:
        return redirect(overview)
    if status.owner == character:
        if not status.read:
            status.read = True
            status.save(update_fields=['read'])

    messages = []
    imessage = status.message
    while True:
        messages.append(imessage)
        imessage = imessage.previous
        if imessage is None:
            break
    context['messages'] = messages
    context['status'] = status

    return render(request, 'extra_mail/view_message.djhtml', context)

def add_contact(request, char_id=None, security_token=None):
    context = {}
    character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    if request.method == 'POST':
        contacts = ContactList.objects.get(owner=character)
        form = AddContactForm(request.POST)
        logging.info(f'Form valid: {form.is_valid()}')
        if form.is_valid():
            try:
                to_add = Character.objects.get(pk=form.cleaned_data['char_id'], security_token=form.cleaned_data['security_token'])
                logging.info(f'Adding {to_add} to {contacts} contact list')
                contacts.contacts.add(to_add)
                contacts.save()
                context['added'] = to_add
                return redirect(contact_list)
            except Character.DoesNotExist:
                context['invalid'] = True

    # New scan
    elif char_id is not None and security_token is not None:
        contacts = ContactList.objects.get(owner=character)
        try:
            to_add = Character.objects.get(pk=char_id, security_token=security_token)
            logging.info(f'Adding {to_add} to {contacts} contact list')
            contacts.contacts.add(to_add)
            contacts.save()
            context['added'] = to_add

            return redirect(contact_list)
        except Character.DoesNotExist:
          context['invalid'] = True
    else:
        context['form'] = AddContactForm()

    return render(request, 'extra_mail/add_contact.djhtml', context)


def send_message(request, from_id=None):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character
    contact_list = ContactList.objects.get(owner=character)

    if request.method == 'POST':
        form = SendMessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.by = character
            message.save()
            message.to.add(*form.cleaned_data['to'])
            message.cci.add(*form.cleaned_data['cci'])
            message.save()
            return redirect(overview, from_id)

    form = SendMessageForm()

    if request.user.is_staff:
        form.fields['to'].queryset = Character.objects.filter(deactivated=False)
        form.fields['cci'].queryset = Character.objects.filter(deactivated=False)
    else:
        form.fields['to'].queryset = contact_list.contacts.all()
        form.fields['cci'].queryset = contact_list.contacts.all()

    context['form'] = form

    return render(request, 'extra_mail/send_message.djhtml', context)

def reply(request, message_id, from_id=None):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character
    contact_list = ContactList.objects.get(owner=character)

    original_message = Message.objects.get(id=message_id)

    if request.method == 'POST':
        form = ReplyMessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.by = character
            message.save()
            message.to.add(original_message.by)
            message.previous = original_message
            message.save()
            return redirect(overview, from_id)

    form = ReplyMessageForm(initial={'subject': f'RE: {original_message.subject}'})
    context['to'] = original_message.by
    context['reply'] = True
    context['form'] = form
    context['message_id'] = message_id

    return render(request, 'extra_mail/send_message.djhtml', context)


def reply_all(request, message_id, from_id=None):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character
    contact_list = ContactList.objects.get(owner=character)

    original_message = Message.objects.get(id=message_id)

    dest = [original_message.by]
    for icharacter in original_message.to.all():
        if icharacter != character:
            dest.append(icharacter)

    if request.method == 'POST':
        form = ReplyMessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.by = character
            message.save()
            for idest in dest:
                message.to.add(idest)
            message.previous = original_message
            message.save()
            return redirect(overview, from_id)

    form = ReplyMessageForm(initial={'subject': f'RE: {original_message.subject}'})
    context['to'] = dest
    context['replyall'] = True
    context['form'] = form
    context['message_id'] = message_id

    return render(request, 'extra_mail/send_message.djhtml', context)

def forward(request, message_id, from_id=None):
    context = {}

    if from_id and from_id != 'None':
        if not request.user.is_staff:
            return redirect(char_views.view_login)
        else:
            character = Character.objects.get(id=from_id)
            context['from_id'] = from_id
    else:
        character = get_character(request)
    if character.is_locked:
        return redirect(char_views.account_compromised)

    context['character'] = character
    contact_list = ContactList.objects.get(owner=character)

    original_message = Message.objects.get(id=message_id)

    if request.method == 'POST':
        form = SendMessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.by = character
            message.save()
            message.to.add(*form.cleaned_data['to'])
            message.previous = original_message
            message.save()
            return redirect(overview, from_id)

    form = SendMessageForm(initial={'subject': f'FW: {original_message.subject}'})

    if request.user.is_staff:
        form.fields['to'].queryset = Character.objects.all()
        form.fields['cci'].queryset = Character.objects.all()
    else:
        form.fields['cci'].queryset = contact_list.contacts.all()
        form.fields['to'].queryset = contact_list.contacts.all()

    context['form'] = form
    context['forward'] = True
    context['message_id'] = message_id

    return render(request, 'extra_mail/send_message.djhtml', context)
