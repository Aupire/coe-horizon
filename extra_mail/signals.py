
from django.db.models.signals import post_save
from django.dispatch import receiver

from character.models import Character
from extra_mail.models import ContactList, Message, MessageStatus


# Signals
@receiver(post_save, sender=Character)
def ensure_character_has_contact_list(sender, instance, created, raw, using, update_fields, **kwargs):
    # If character has no contact list, create one
    try:
        contact_list = ContactList.objects.get(owner=instance)
    except:
        contact_list = ContactList(owner=instance)
        contact_list.save()


@receiver(post_save, sender=Message)
def ensure_message_recipient_has_status(sender, instance, created, raw, using, update_fields, **kwargs):
    for recipient in instance.to.all():
        try:
            MessageStatus.objects.get(owner=recipient, message=instance)
        except:
            status = MessageStatus(owner=recipient, message=instance, timestamp=instance.timestamp)
            status.save()

    for recipient in instance.cci.all():
        try:
            MessageStatus.objects.get(owner=recipient, message=instance)
        except:
            status = MessageStatus(owner=recipient, message=instance, timestamp=instance.timestamp)
            status.save()
